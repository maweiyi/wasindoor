package com.aiyc.framework.component;

public interface ICloneable
extends Cloneable
{

public abstract Object clone()
    throws CloneNotSupportedException;
}
